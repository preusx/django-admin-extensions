# Django admin extensions

Utilities and extensions for django administration panel.

Docs here: [https://preusx.gitlab.io/django-admin-extensions](https://preusx.gitlab.io/django-admin-extensions)
