from django.utils.translation import gettext_lazy as _


JET_DEFAULT_THEME = 'default'
JET_SIDE_MENU_COMPACT = True

JET_SIDE_MENU_ITEMS = (
    {'label': _('Example'), 'items': (
        {'name': 'example.allfields'},
    )},

    {'label': _('Admin users'), 'items': (
        {'name': 'auth.user'},
        {'name': 'auth.group'},
    )},
)
