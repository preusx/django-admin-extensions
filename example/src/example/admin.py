from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.db.models.functions import Coalesce, Trunc
from admin_numeric_filter.admin import SliderNumericFilter
from rangefilter.filters import DateTimeRangeFilter
from pxd_admin_extensions.contrib.all import AllAdmin
from pxd_admin_extensions.contrib.rangefilter.filters import DateTimeRangeForRangeFilter
from pxd_admin_extensions import TextInputFilter
from pxd_admin_extensions import MethodFilter
from pxd_admin_extensions.contrib.stats import (
    register_stats, stat, stat_legend
)

from .models import AllFields


@admin.register(AllFields)
class AllFieldsAdmin(AllAdmin):
    list_display = (
        'title',
        'amount', 'count',
        'is_disabled',
        'identifiers_list', 'get_identifiers_count',
        'created_at',
    )
    list_annotations = (
        (
            'identifiers_list',
            lambda field: models.Func(models.F(field), function='CARDINALITY'),
            'identifiers_count'
        ),
    )
    list_totals = (
        (
            'get_identifiers_count',
            lambda field: Coalesce(models.Sum('identifiers_count'), 0)
        ),
    )
    list_display_links = ('title',)
    list_filter = (
        'is_disabled',
        ('title', TextInputFilter),
        ('amount', SliderNumericFilter),
        ('created_at', DateTimeRangeFilter),
        MethodFilter.as_filter('none', title=_('Empty result')),
        'created_at',
    )
    search_fields = ('title', 'body')
    date_hierarchy = 'created_at'
    empty_value_display = '-----'

    def get_identifiers_count(self, obj):
        return obj.identifiers_count


@register_stats(AllFieldsAdmin, model=AllFields)
class AllFieldsStatsAdmin:
    @stat(_('Count of disabled/enabled'), (
        stat_legend('is_disabled', _('Disabled')),
        stat_legend('all_count', _('Count counts')),
    ))
    def get_enabled_counts(self, request, qs):
        return (
            qs
            .values('is_disabled')
            .annotate(all_count=Coalesce(models.Sum('count'), 0))
            .order_by('is_disabled')
        )

    @stat(_('Amounts timeline'), (
        stat_legend('time_point', _('Time')),
        stat_legend('amount_sum', _('Amount')),
    ), config={'dates': ['time_point'], 'type': 'line'})
    def get_amount_timeline(self, request, qs):
        return (
            qs
            .annotate(amount_sum=models.Sum('amount'))
            .annotate(time_point=Trunc(
                'created_at', self.get_stats_date_period(request)
            ))
            .values('time_point', 'amount_sum')
            .order_by('time_point')
        )
