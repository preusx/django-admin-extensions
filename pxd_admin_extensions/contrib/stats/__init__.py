from .const import *
from .admin import *
from .factories import *
from .utils import *


default_app_config = (
    'pxd_admin_extensions.contrib.stats.apps.StatsConfig'
)
