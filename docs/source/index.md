# Django admin extensions

## Installation

```bash
pip install preusx-django-admin-extensions[contrib]
```

Where:

- contrib - All used libraries dependencies.

It also depends on:

- Admin totals: `admin-totals @ git+https://git@github.com/douwevandermeij/admin-totals.git#egg=admin-totals`. This one is **required** if you're going to use `contrib`.
- Django Jet: `django-jet @ git+https://git@github.com/preusx/django-jet.git#egg=django-jet`. If you need jet functionality in your project.

Default admin panel consists of several own packages and dependencies. They may be used separately except `pxd_admin_extensions.contrib.all` which is the aggregator-package for all extensions.

```python
INSTALLED_APPS = [
  'pxd_admin_extensions.contrib.all',
  'pxd_admin_extensions.contrib.jet',
  'pxd_admin_extensions.contrib.stats',
  'pxd_admin_extensions.contrib.rangefilter',
  'pxd_admin_extensions',

  'django_better_admin_arrayfield',
  'admin_totals',
  'admin_numeric_filter',
  'jet.dashboard',
  'jet',
] + INSTALLED_APPS
```

After installation it's easy to use:

```python
from pxd_admin_extensions.contrib.all import AllAdmin
...


@admin.register(SomeModel)
class SomeModelAdmin(AllAdmin):
  pass
```

Project has a simple `example` app for a demonstration purposes.
