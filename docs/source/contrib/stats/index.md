# Stats

Simple extension to add statistics display to default django administrative app. Almost with no default templates change.

???+ warning
    For now it's awailable only for jet admin.

    For default admin package there must be made `stats_change_list.html` template.

It uses nv.d3 package to draw svg charts on the stats page.

For custom template(if you're going to made one) it's enough to add a small bit of code to add link to a stats page:

```jinja
{% extends "admin/change_list.html" %}

{% block object-tools-items %}
  {{ block.super }}
  {%- include "admin/includes/stats/stats_link.html" %}
{% endblock %}
```

To add stat page to a model is very simple:
```python
from django.contrib import admin


@admin.register(ExistingModel)
class ExistingModelAdmin(admin.ModelAdmin):
    pass


@register_stats(ExistingModelAdmin, model=ExistingModel)
class ExistingModelStatsAdmin:
    @stat(_('Participants per domain'), (
        stat_legend('domain', _('Domain')),
        stat_legend('participants_count', _('Participants count')),
    ), config={'type': 'pie', 'labelType': 'percent'})
    def get_participants_per_domain(self, request, qs):
        return (
            qs
            .values('domain')
            .annotate(participants_count=models.Count(
                'participants__participant_id', distinct=True
            ))
            .order_by('domain')
        )
```

`ExistingModelStatsAdmin` - Will extend `ExistingModelAdmin` and `StatsAdmin` with all it's filters and other specificities.

For every `@stat`-wrapped method it will create chart block. All those charts will be affected by the applied filters(the `qs` parameter of the stat method will be already "transformed").
