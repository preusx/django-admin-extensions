# ManyToMany changelist editable fields

Example:

```python
from django.contrib import admin

from django import forms
from pxd_admin_extensions import manytomany_changelist

from .models import Product, Country


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
  list_display = [
    'id',
    'title',
    'brand',
  ]

  get_changelist_form, get_changelist = manytomany_changelist.make_admin_methods(
    # Pass a list of fields that should be editable in the changelist.
    # You could define not only a many-to-many field, but also any
    # regular field.
    [
      (
        # Model field name.
        'countries',
        # Form field.
        forms.ModelMultipleChoiceField(
          queryset=Country.objects.all(), required=False
        ),
        # Special "injector" function, that will insert fields to a
        # `list_display` config.
        # May be ommitted and default append function will be used.
        manytomany_changelist.after_injector('title')
      ),
    ],
  )
```

::: pxd_admin_extensions.manytomany_changelist
