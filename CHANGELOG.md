# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.5]
### Fixed
- Many to many admin changelist injector issue fix.

## [0.1.4]
### Added
- Many to many admin changelist editable fields.

## [0.1.1]
Initial version.
